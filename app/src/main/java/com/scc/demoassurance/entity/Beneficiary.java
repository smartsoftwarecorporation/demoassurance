package com.scc.demoassurance.entity;

/**
 * Created by LMT on 07/02/2017.
 */

public class Beneficiary {

    private String name;
    private String surname;
    private String tel;
    private String cni_password;
    private int id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCni_password() {
        return cni_password;
    }

    public void setCni_password(String cni_password) {
        this.cni_password = cni_password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
