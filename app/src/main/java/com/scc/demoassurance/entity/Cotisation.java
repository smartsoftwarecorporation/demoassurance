package com.scc.demoassurance.entity;

import java.util.Date;

/**
 * Created by LMT on 07/02/2017.
 */

public class Cotisation {

    private int id;
    private String idPolicy;
    private String telPayement;
    private String amount;
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdPolicy() {
        return idPolicy;
    }

    public void setIdPolicy(String idPolicy) {
        this.idPolicy = idPolicy;
    }

    public String getTelPayement() {
        return telPayement;
    }

    public void setTelPayement(String telPayement) {
        this.telPayement = telPayement;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
