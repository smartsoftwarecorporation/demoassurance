package com.scc.demoassurance.entity;

/**
 * Created by LMT on 07/02/2017.
 */

public class Subscriber {

    private String name;
    private String surname;
    private String tel;
    private String cni_password;
    private String matricule;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCni_password() {
        return cni_password;
    }

    public void setCni_password(String cni_password) {
        this.cni_password = cni_password;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }
}
