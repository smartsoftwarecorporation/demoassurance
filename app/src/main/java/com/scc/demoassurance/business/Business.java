package com.scc.demoassurance.business;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.scc.demoassurance.entity.Beneficiary;
import com.scc.demoassurance.entity.Cotisation;
import com.scc.demoassurance.entity.Policy;
import com.scc.demoassurance.entity.Subscriber;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by LMT on 07/02/2017.
 */

public class Business {

    public final static String PREF_SUBSCRIBER = "PREF_SUBSCRIBER";
    public final static String PREF_ARRAY_BENEFICIARY = "PREF_ARRAY_BENEFICIARY";
    public final static String PREF_ARRAY_POLICY = "PREF_ARRAY_POLICY";
    public final static String PREF_ARRAY_COTISATION = "PREF_ARRAY_COTISATION";

    public void beneficiaryCreate(Beneficiary beneficiary, SharedPreferences setting) throws JSONException {

        Gson gson = new Gson();

        String sBeneficiary = gson.toJson(beneficiary);

        String sArrayBeneficiary = setting.getString(Business.PREF_ARRAY_BENEFICIARY, "");

        JSONArray arrayBeneficiary;

        if (sArrayBeneficiary.isEmpty())
            arrayBeneficiary = new JSONArray();
        else
            arrayBeneficiary = new JSONArray(sArrayBeneficiary);

        arrayBeneficiary.put(new JSONObject(sBeneficiary));

        SharedPreferences.Editor editor = setting.edit();

        editor.putString(PREF_ARRAY_BENEFICIARY, arrayBeneficiary.toString());
        editor.apply();
        editor.commit();

    }

    public Beneficiary beneficiaryFindById(int id, SharedPreferences setting) throws JSONException {

        Gson gson = new Gson();

        String sArrayBeneficiary = setting.getString(Business.PREF_ARRAY_BENEFICIARY, "");

        JSONArray arrayBeneficiary;

        if (sArrayBeneficiary.isEmpty())
            arrayBeneficiary = new JSONArray();
        else
            arrayBeneficiary = new JSONArray(sArrayBeneficiary);

        for (int i = 0; i < arrayBeneficiary.length(); i++) {

            Beneficiary beneficiary = gson.fromJson(arrayBeneficiary.getJSONObject(i).toString(), Beneficiary.class);

            if (beneficiary.getId() == id)
                return beneficiary;
        }

        return null;
    }

    public Beneficiary beneficiaryFindFromPolicy(String idPolicy, SharedPreferences setting) throws JSONException {

        String sArrayPolicy = setting.getString(Business.PREF_ARRAY_POLICY, "");

        JSONArray arrayPolicy;

        if (sArrayPolicy.isEmpty())
            arrayPolicy = new JSONArray();
        else
            arrayPolicy = new JSONArray(sArrayPolicy);


        Gson gson = new Gson();

        for (int i = 0; i < arrayPolicy.length(); i++) {

            Policy policy = gson.fromJson(arrayPolicy.getJSONObject(i).toString(), Policy.class);

            if (policy.getId().equals(idPolicy)) {

                return beneficiaryFindById(policy.getIdBeneficiary(), setting);
            }

        }

        return null;
    }

    public void subscriberCreate(Subscriber subscriber, SharedPreferences setting) throws JSONException {

        Gson gson = new Gson();

        String sSubscriber = gson.toJson(subscriber);

        SharedPreferences.Editor editor = setting.edit();

        editor.putString(PREF_SUBSCRIBER, sSubscriber);
        editor.apply();
        editor.commit();

    }

    public Policy policyCreate(Subscriber subscriber, Beneficiary beneficiary, SharedPreferences setting) throws JSONException {

        Gson gson = new Gson();

        Policy policy = new Policy();

        policy.setId(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        policy.setCreationDate(new Date());
        policy.setIdBeneficiary(beneficiary.getId());
        policy.setIdSubscriber(subscriber.getMatricule());

        String sPolicy = gson.toJson(policy);

        String sArrayPolicy = setting.getString(Business.PREF_ARRAY_POLICY, "");

        JSONArray arrayPolicy;

        if (sArrayPolicy.isEmpty())
            arrayPolicy = new JSONArray();
        else
            arrayPolicy = new JSONArray(sArrayPolicy);

        arrayPolicy.put(new JSONObject(sPolicy));

        SharedPreferences.Editor editor = setting.edit();

        editor.putString(PREF_ARRAY_POLICY, arrayPolicy.toString());
        editor.apply();
        editor.commit();

        return policy;
    }


    public Policy policyCreate(String jsonInfos, String identifiant, SharedPreferences setting) throws JSONException {

        Gson gson = new Gson();

        Beneficiary beneficiary = new Beneficiary();
        Subscriber subscriber = new Subscriber();

        beneficiary.setCni_password("478898565");
        beneficiary.setName("testNameBene");
        beneficiary.setSurname("testSurnameBene");
        beneficiary.setTel("691289877");
        beneficiary.setId((int) (long) Calendar.getInstance().getTimeInMillis());

        beneficiaryCreate(beneficiary, setting);

        subscriber.setMatricule(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        subscriber.setTel("698879417");
        subscriber.setSurname("testSurnameSub");
        subscriber.setName("testNameSub");
        subscriber.setCni_password("898564112");

        subscriberCreate(subscriber, setting);


        Policy policy = new Policy();

        policy.setId(identifiant);
        policy.setCreationDate(new Date());
        policy.setIdBeneficiary(beneficiary.getId());
        policy.setIdSubscriber(subscriber.getMatricule());

        String sPolicy = gson.toJson(policy);

        String sArrayPolicy = setting.getString(Business.PREF_ARRAY_POLICY, "");

        JSONArray arrayPolicy;

        if (sArrayPolicy.isEmpty())
            arrayPolicy = new JSONArray();
        else
            arrayPolicy = new JSONArray(sArrayPolicy);

        arrayPolicy.put(new JSONObject(sPolicy));

        SharedPreferences.Editor editor = setting.edit();

        editor.putString(PREF_ARRAY_POLICY, arrayPolicy.toString());
        editor.apply();
        editor.commit();

        return policy;

    }


    public List<String> policyFindAll(SharedPreferences setting) throws JSONException {

        String sArrayPolicy = setting.getString(Business.PREF_ARRAY_POLICY, "");

        JSONArray arrayPolicy;

        if (sArrayPolicy.isEmpty())
            arrayPolicy = new JSONArray();
        else
            arrayPolicy = new JSONArray(sArrayPolicy);

        List<String> list = new ArrayList<>();

        Gson gson = new Gson();

        for (int i = 0; i < arrayPolicy.length(); i++) {

            Policy policy = gson.fromJson(arrayPolicy.getJSONObject(i).toString(), Policy.class);

            list.add(policy.getId());
        }


        return list;

    }

    public void cotisationCreate(Cotisation cotisation, SharedPreferences setting) throws JSONException {

        cotisation.setId((int) (long) Calendar.getInstance().getTimeInMillis());

        String sArrayCotisation = setting.getString(Business.PREF_ARRAY_COTISATION, "");

        JSONArray arrayCotisation;

        if (sArrayCotisation.isEmpty())
            arrayCotisation = new JSONArray();
        else
            arrayCotisation = new JSONArray(sArrayCotisation);


        Gson gson = new Gson();

        String sCotisation = gson.toJson(cotisation);

        arrayCotisation.put(new JSONObject(sCotisation));

        SharedPreferences.Editor editor = setting.edit();

        editor.putString(PREF_ARRAY_COTISATION, arrayCotisation.toString());
        editor.apply();
        editor.commit();

    }

    public List<Cotisation> cotisationFindAll(SharedPreferences setting) throws JSONException {

        List<Cotisation> cotisations = new ArrayList<>();

        String sArrayCotisation = setting.getString(Business.PREF_ARRAY_COTISATION, "");

        JSONArray arrayCotisation;

        if (sArrayCotisation.isEmpty())
            arrayCotisation = new JSONArray();
        else
            arrayCotisation = new JSONArray(sArrayCotisation);


        Gson gson = new Gson();

        for (int i = 0; i < arrayCotisation.length(); i++) {

            Cotisation cotisation = gson.fromJson(arrayCotisation.getJSONObject(i).toString(), Cotisation.class);

            cotisations.add(cotisation);
        }


        return cotisations;
    }

}
