package com.scc.demoassurance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.scc.demoassurance.R;
import com.scc.demoassurance.entity.Cotisation;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by LMT on 08/02/2017.
 */

public class CotisationAdapter extends ArrayAdapter<Cotisation> {

    private List<Cotisation> objects;


    public CotisationAdapter(Context context, List<Cotisation> objects) {
        super(context, R.layout.item_cotisation, objects);

        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) super.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_cotisation, parent, false);

        TextView tvDate = (TextView) rowView.findViewById(R.id.tvDate);
        TextView tvMontant = (TextView) rowView.findViewById(R.id.tvMontant);
        TextView tvPoilce = (TextView) rowView.findViewById(R.id.tvPolice);
        TextView tvTel = (TextView) rowView.findViewById(R.id.tvTel);

        Cotisation cotisation = objects.get(position);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRENCH);

        tvDate.setText(dateFormat.format(cotisation.getDate()));
        tvMontant.setText(cotisation.getAmount() + " Fcfa");
        tvPoilce.setText(cotisation.getIdPolicy());
        tvTel.setText(cotisation.getTelPayement());

        return rowView;
    }
}
