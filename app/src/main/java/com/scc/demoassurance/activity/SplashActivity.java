package com.scc.demoassurance.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.scc.demoassurance.R;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME = 800;// 0.5 seconds
    private static final String FIRST_START = "FIRST_START";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences setting;
        setting = PreferenceManager
                .getDefaultSharedPreferences(SplashActivity.this);
        String appStart = setting.getString(FIRST_START, "");

        if (appStart.isEmpty()) {

            SharedPreferences.Editor editor = setting
                    .edit();
            editor.putString(FIRST_START, "STARTED");
            editor.apply();
            editor.commit();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    intent.putExtra("notificationReceiverPaiement", 1);
                    startActivity(intent);

                    SplashActivity.this.finish();

                    overridePendingTransition(R.transition.fade_in, R.transition.fade_out);

                }
            }, SPLASH_TIME);

        } else {

            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
            intent.putExtra("notificationReceiverPaiement", 1);
            startActivity(intent);

            SplashActivity.this.finish();
        }


    }


}
