package com.scc.demoassurance.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.scc.demoassurance.R;

public class ProductInfosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_infos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);


        if (toolbar != null) {

            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
            toolbar.setTitle("Demo Assurance");
            toolbar.setSubtitle("Information sur la police");

        }
    }
}
