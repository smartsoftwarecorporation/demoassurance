package com.scc.demoassurance.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.EditText;

import com.google.gson.Gson;
import com.scc.demoassurance.R;
import com.scc.demoassurance.business.Business;
import com.scc.demoassurance.entity.Beneficiary;
import com.scc.demoassurance.entity.Policy;
import com.scc.demoassurance.entity.Subscriber;

import org.json.JSONException;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewClientActivity extends AppCompatActivity {


    @BindView(R.id.edNameSouscripteur)
    EditText edNameSouscripteur;
    @BindView(R.id.edSurnameSouscripteur)
    EditText edSurnameSouscripteur;
    @BindView(R.id.edCniSouscripteur)
    EditText edCniSouscripteur;
    @BindView(R.id.edTelSouscripteur)
    EditText edTelSouscripteur;
    @BindView(R.id.edNameBene)
    EditText edNameBene;
    @BindView(R.id.edSurnameBene)
    EditText edSurnameBene;
    @BindView(R.id.edCniBene)
    EditText edCniBene;
    @BindView(R.id.edTelBene)
    EditText edTelBene;


    SharedPreferences setting;
    Subscriber mSubscriber;
    Business business;
    Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_client);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);


        if (toolbar != null) {

            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");
            toolbar.setTitle("Demo Assurance");
            toolbar.setSubtitle("Souscription à la police");

        }

        business = new Business();

        gson = new Gson();

        setting = PreferenceManager.getDefaultSharedPreferences(NewClientActivity.this);

        String sSubscriber = setting.getString(Business.PREF_SUBSCRIBER, "");

        mSubscriber = null;

        if (!sSubscriber.isEmpty()) {
            mSubscriber = gson.fromJson(sSubscriber, Subscriber.class);

            if (mSubscriber != null) {

                edCniSouscripteur.setText(mSubscriber.getCni_password());
                edNameSouscripteur.setText(mSubscriber.getName());
                edSurnameSouscripteur.setText(mSubscriber.getSurname());
                edTelSouscripteur.setText(mSubscriber.getTel());

                edCniSouscripteur.setEnabled(false);
                edTelSouscripteur.setEnabled(false);
                edSurnameSouscripteur.setEnabled(false);
                edNameSouscripteur.setEnabled(false);
            }
        }


    }

    private boolean validationForm() {

        boolean valid = true;

        EditText[] eTformEntry = {edCniBene, edCniSouscripteur, edNameBene,
                edNameSouscripteur, edNameSouscripteur, edTelBene, edTelSouscripteur, edSurnameBene};

        for (EditText editText : eTformEntry) {

            switch (editText.getId()) {
                case R.id.edNameSouscripteur:

                    if (edNameSouscripteur.getText().toString().isEmpty()) {
                        edNameSouscripteur.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edNameSouscripteur.setError(null);

                    break;
                case R.id.edSurnameSouscripteur:
                    if (edSurnameSouscripteur.getText().toString().isEmpty()) {
                        edSurnameSouscripteur.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edSurnameSouscripteur.setError(null);
                    break;
                case R.id.edCniSouscripteur:
                    if (edCniSouscripteur.getText().toString().isEmpty()) {
                        edCniSouscripteur.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edCniSouscripteur.setError(null);
                    break;
                case R.id.edTelSouscripteur:
                    if (edTelSouscripteur.getText().toString().isEmpty()) {
                        edTelSouscripteur.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edTelSouscripteur.setError(null);
                    break;
                case R.id.edNameBene:
                    if (edNameBene.getText().toString().isEmpty()) {
                        edNameBene.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edNameBene.setError(null);
                    break;
                case R.id.edCniBene:
                    if (edCniBene.getText().toString().isEmpty()) {
                        edCniBene.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edCniBene.setError(null);
                    break;
                case R.id.edTelBene:
                    if (edTelBene.getText().toString().isEmpty()) {
                        edTelBene.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edTelBene.setError(null);
                    break;
                case R.id.edSurnameBene:
                    if (edSurnameBene.getText().toString().isEmpty()) {
                        edSurnameBene.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edSurnameBene.setError(null);
                    break;

            }
        }

        return valid;
    }

    @OnClick(R.id.btValiderCreation)
    public void onClickValidationButton() {

        if (validationForm()) {

            Beneficiary beneficiary = new Beneficiary();
            Subscriber subscriber = new Subscriber();

            beneficiary.setCni_password(edCniBene.getText().toString());
            beneficiary.setName(edNameBene.getText().toString());
            beneficiary.setSurname(edSurnameBene.getText().toString());
            beneficiary.setTel(edTelBene.getText().toString());

            subscriber.setTel(edTelSouscripteur.getText().toString());
            subscriber.setSurname(edSurnameSouscripteur.getText().toString());
            subscriber.setName(edNameSouscripteur.getText().toString());
            subscriber.setCni_password(edCniSouscripteur.getText().toString());

            AccountCreation accountCreation = new AccountCreation(beneficiary, subscriber);

            accountCreation.execute();


        }

    }

    class AccountCreation extends AsyncTask<String, String, String> {

        ProgressDialog pDialog;

        private Beneficiary beneficiary;
        private Subscriber subscriber;

        AccountCreation(Beneficiary beneficiary, Subscriber subscriber) {
            this.beneficiary = beneficiary;
            this.subscriber = subscriber;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();


            pDialog = new ProgressDialog(NewClientActivity.this);

            WindowManager.LayoutParams wmlp = pDialog.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER;

            pDialog.setMessage("Création du compte en cours...");
            pDialog.setCancelable(false);
            pDialog.setIndeterminate(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            pDialog.show();
            //pDialog.setContentView(R.layout.progress_dialog_layout);
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "success";
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);


            beneficiary.setId((int) (long) Calendar.getInstance().getTimeInMillis());
            subscriber.setMatricule(String.valueOf(Calendar.getInstance().getTimeInMillis()));

            Business business = new Business();

            SharedPreferences setting;
            setting = PreferenceManager
                    .getDefaultSharedPreferences(NewClientActivity.this);

            Policy policy = new Policy();
            try {


                business.beneficiaryCreate(beneficiary, setting);

                if (mSubscriber != null) {
                    subscriber = mSubscriber;
                }
                business.subscriberCreate(subscriber, setting);

                policy = business.policyCreate(subscriber, beneficiary, setting);

            } catch (JSONException e) {
                e.printStackTrace();
                result = "error";
            }

            pDialog.dismiss();

            switch (result) {

                case "success":
                    new AlertDialog.Builder(NewClientActivity.this)
                            .setTitle("Compte Crée")
                            .setMessage("Votre compte a été créé.\n Identifiant : " + policy.getId() + "\n Bien vouloir le noter")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    finish();
                                }
                            })
                            .setCancelable(false)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    break;

                case "error":
                    new AlertDialog.Builder(NewClientActivity.this)
                            .setTitle("Problème")
                            .setMessage("Une erreur est survenue lors de la création de votre compte")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setCancelable(true)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    break;

                default:
                    break;

            }


        }


    }


}
