package com.scc.demoassurance.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.scc.demoassurance.R;
import com.scc.demoassurance.business.Business;
import com.scc.demoassurance.entity.Subscriber;

import org.json.JSONException;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {


    @BindView(R.id.button_new_client)
    Button button_new_client;

    @BindView(R.id.button_allready_client)
    Button button_allready_client;

    @BindView(R.id.button_about_product)
    Button button_about_product;

    @BindView(R.id.tool_bar)
    Toolbar toolbar;

    SharedPreferences setting;
    Subscriber mSubscriber;
    Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        gson = new Gson();

        setting = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);

        String sSubscriber = setting.getString(Business.PREF_SUBSCRIBER, "");

        mSubscriber = null;

        if (!sSubscriber.isEmpty()) {
            mSubscriber = gson.fromJson(sSubscriber, Subscriber.class);
        }

        //toolbar = (Toolbar) findViewById(R.id.tool_bar);

        if (toolbar != null) {

            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");

            toolbar.setTitle("Demo Assurance");

            if (mSubscriber == null)
                toolbar.setSubtitle("");
            else {

                toolbar.setSubtitle("Bienvenue " + mSubscriber.getSurname().toLowerCase(Locale.FRENCH));
                changeButtonText();
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSubscriber == null) {

            String sSubscriber = setting.getString(Business.PREF_SUBSCRIBER, "");

            if (!sSubscriber.isEmpty()) {

                mSubscriber = gson.fromJson(sSubscriber, Subscriber.class);

                if (mSubscriber != null) {

                    changeButtonText();

                    toolbar.setSubtitle("Bienvenue " + mSubscriber.getSurname().toLowerCase(Locale.FRENCH));
                }

            }


        }


    }

    private void changeButtonText() {
        //important

        button_new_client.setText("Payer votre prime");
        button_allready_client.setText("Nouveau bénéficaire");
        button_about_product.setText("Mes transactions");

    }

    @OnClick(R.id.button_new_client)
    public void onClickBtNewClient() {

        if (mSubscriber == null)
            //nouveau client
            startActivity(new Intent(HomeActivity.this, NewClientActivity.class));
        else
            //payer prime
            startActivity(new Intent(HomeActivity.this, CotiserActivity.class));
    }


    @OnClick(R.id.button_allready_client)
    public void onClickBtAllreadyClient() {

        if (mSubscriber == null)
            //déjà client
            allReadyClient();

        else
            //Nouveau bénéficiaire
            startActivity(new Intent(HomeActivity.this, NewClientActivity.class));
    }

    @OnClick(R.id.button_about_product)
    public void onClickBtAboutProduct() {

        if (mSubscriber == null)
            startActivity(new Intent(HomeActivity.this, ProductInfosActivity.class));

        else
            startActivity(new Intent(HomeActivity.this, TransactionsActivity.class));
    }

    private void allReadyClient() {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);

        final EditText input = new EditText(this);
        input.setHint("identifiant");
        input.setInputType(InputType.TYPE_CLASS_NUMBER);

        alertDialog.setTitle("Identification");
        alertDialog.setMessage("Entrer votre identifiant d'assurance");
        alertDialog.setView(input);
        alertDialog.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete

                if (!input.getText().toString().isEmpty()) {
                    input.setError(null);

                    dialog.dismiss();

                    new SynchExistingAccount(input.getText().toString()).execute();

                } else {
                    input.setError("Champ obligatoire");
                }

            }
        });
        alertDialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                dialog.dismiss();

            }
        });
        alertDialog.setCancelable(true);
        alertDialog.setIcon(android.R.drawable.ic_dialog_info);
        alertDialog.show();

    }


    class SynchExistingAccount extends AsyncTask<String, String, String> {

        ProgressDialog pDialog;

        private String identifiant;

        SynchExistingAccount(String identifiant) {
            this.identifiant = identifiant;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();


            pDialog = new ProgressDialog(HomeActivity.this);

            WindowManager.LayoutParams wmlp = pDialog.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER;

            pDialog.setMessage("Vérification du compte en cours...");
            pDialog.setCancelable(false);
            pDialog.setIndeterminate(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            pDialog.show();
            //pDialog.setContentView(R.layout.progress_dialog_layout);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            pDialog.setMessage(values[0]);
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Thread.sleep(1500);
                if (!identifiant.equals("123456789"))
                    return "unknow";

                publishProgress("Compte trouvé synchronisation en cours...");

                Thread.sleep(1500);

                return "success";

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return "error";
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            pDialog.dismiss();

            switch (result) {

                case "success":

                    Business business = new Business();

                    SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);

                    try {
                        business.policyCreate("", identifiant, setting);

                        startActivity(new Intent(HomeActivity.this, CotiserActivity.class));

                    } catch (JSONException e) {
                        e.printStackTrace();

                        new AlertDialog.Builder(HomeActivity.this)
                                .setTitle("Problème")
                                .setMessage("Une erreur est survenue lors de la vérification de votre compte")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .setCancelable(true)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }


                    break;

                case "unknow":

                    new AlertDialog.Builder(HomeActivity.this)
                            .setTitle("Information")
                            .setMessage("indentifiant non reconnnu dans le système")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    dialog.dismiss();
                                }
                            })
                            .setCancelable(true)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    break;

                case "error":
                    new AlertDialog.Builder(HomeActivity.this)
                            .setTitle("Problème")
                            .setMessage("Une erreur est survenue lors de la vérification de votre compte")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    dialog.dismiss();
                                }
                            })
                            .setCancelable(true)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    break;

                default:
                    break;

            }


        }


    }
}
