
package com.scc.demoassurance.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scc.demoassurance.R;
import com.scc.demoassurance.business.Business;
import com.scc.demoassurance.entity.Beneficiary;
import com.scc.demoassurance.entity.Cotisation;
import com.scc.demoassurance.entity.Subscriber;

import org.json.JSONException;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CotiserActivity extends AppCompatActivity {

    @BindView(R.id.spnPolicy)
    Spinner spnPolicy;

    @BindView(R.id.tvNameBeneficiary)
    TextView tvNameBeneficiary;

    @BindView(R.id.edMontant)
    EditText edMontant;

    @BindView(R.id.edTelPayement)
    EditText edTelPayement;

    @BindView(R.id.tool_bar)
    Toolbar toolbar;

    SharedPreferences setting;
    Subscriber mSubscriber;
    Business business;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotiser);

        ButterKnife.bind(this);

        business = new Business();

        gson = new Gson();

        setting = PreferenceManager.getDefaultSharedPreferences(CotiserActivity.this);

        String sSubscriber = setting.getString(Business.PREF_SUBSCRIBER, "");

        mSubscriber = null;

        if (!sSubscriber.isEmpty()) {
            mSubscriber = gson.fromJson(sSubscriber, Subscriber.class);
        }

        try {
            prepareSpinner();
        } catch (JSONException e) {
            e.printStackTrace();
            finish();
        }

        //toolbar = (Toolbar) findViewById(R.id.tool_bar);

        if (toolbar != null) {

            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");

            toolbar.setTitle("Demo Assurance");
            toolbar.setSubtitle("Payement prime");

        }
    }

    @OnClick(R.id.btValiderPaiement)
    public void onClickValiderButton() {

        if (validationForm()) {

            Cotisation cotisation = new Cotisation();

            cotisation.setAmount(edMontant.getText().toString());
            cotisation.setDate(new Date());
            cotisation.setIdPolicy(spnPolicy.getSelectedItem().toString());
            cotisation.setTelPayement(edTelPayement.getText().toString());

            new MakeCotisation(cotisation).execute();

        }

    }

    private void prepareSpinner() throws JSONException {

        final List<String> list = business.policyFindAll(setting);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnPolicy.setAdapter(dataAdapter);

        spnPolicy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    Beneficiary beneficiary = business.beneficiaryFindFromPolicy(list.get(position), setting);

                    if (beneficiary != null) {
                        tvNameBeneficiary.setText(beneficiary.getName() + " " + beneficiary.getSurname());
                    } else
                        tvNameBeneficiary.setText("");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private boolean validationForm() {

        boolean valid = true;

        EditText[] eTformEntry = {edMontant, edTelPayement};

        for (EditText editText : eTformEntry) {

            switch (editText.getId()) {
                case R.id.edTelPayement:

                    if (edTelPayement.getText().toString().isEmpty()) {
                        edTelPayement.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edTelPayement.setError(null);

                    break;
                case R.id.edMontant:
                    if (edMontant.getText().toString().isEmpty()) {
                        edMontant.setError("Champs Obligatoire");
                        valid = false;
                    } else
                        edMontant.setError(null);
                    break;


            }
        }

        return valid;
    }

    class MakeCotisation extends AsyncTask<String, String, String> {

        ProgressDialog pDialog;

        private Cotisation cotisation;

        MakeCotisation(Cotisation cotisation) {
            this.cotisation = cotisation;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();


            pDialog = new ProgressDialog(CotiserActivity.this);

            WindowManager.LayoutParams wmlp = pDialog.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER;

            pDialog.setMessage("Paiement en cours...");
            pDialog.setCancelable(false);
            pDialog.setIndeterminate(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            pDialog.show();
            //pDialog.setContentView(R.layout.progress_dialog_layout);
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Thread.sleep(2500);

                business.cotisationCreate(cotisation, setting);

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();

                return "error";
            }

            return "success";
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            pDialog.dismiss();

            switch (result) {

                case "success":
                    new AlertDialog.Builder(CotiserActivity.this)
                            .setTitle("Efectué")
                            .setMessage("Paiement de votre prime d'assurance effectué")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    dialog.dismiss();
                                    finish();
                                }
                            })
                            .setCancelable(false)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    break;

                case "error":
                    new AlertDialog.Builder(CotiserActivity.this)
                            .setTitle("Problème")
                            .setMessage("Une erreur est survenue lors du paiement")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setCancelable(true)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    break;

                default:
                    break;

            }


        }


    }


}
