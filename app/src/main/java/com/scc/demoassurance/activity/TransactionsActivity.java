package com.scc.demoassurance.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.google.gson.Gson;
import com.scc.demoassurance.R;
import com.scc.demoassurance.adapter.CotisationAdapter;
import com.scc.demoassurance.business.Business;
import com.scc.demoassurance.entity.Subscriber;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionsActivity extends AppCompatActivity {


    @BindView(R.id.lvTransaction)
    ListView lvTransaction;

    @BindView(R.id.tool_bar)
    Toolbar toolbar;

    SharedPreferences setting;
    Subscriber mSubscriber;
    Business business;
    Gson gson;

    CotisationAdapter cotisationAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        ButterKnife.bind(this);

        business = new Business();

        gson = new Gson();

        setting = PreferenceManager.getDefaultSharedPreferences(TransactionsActivity.this);

        String sSubscriber = setting.getString(Business.PREF_SUBSCRIBER, "");

        mSubscriber = null;

        if (!sSubscriber.isEmpty()) {
            mSubscriber = gson.fromJson(sSubscriber, Subscriber.class);
        }
        if (toolbar != null) {

            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("");

            toolbar.setTitle("Demo Assurance");
            toolbar.setSubtitle("Liste des transactions");

        }
        try {
            cotisationAdapter = new CotisationAdapter(TransactionsActivity.this, business.cotisationFindAll(setting));
            lvTransaction.setAdapter(cotisationAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
            finish();
        }
    }
}
